# Meringue

Package with various functional (such as mixins, form utils, upload handlers and other) for Django Framework<br />
documentation - http://pythonhosted.org/Meringue/<br />
code - http://code.weboven.net/weboven_team/meringue/


## requirements

need python version 3.6 or later and Django v1.10.6+
